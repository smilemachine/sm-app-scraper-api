'use strict';

/*
 * Returns a parseArchive function as a Promise
 */
function getParseArchive (opts) {
 return ($) => Promise.resolve(parseArchive($))
}

/*
 * Returns a Promise of the app details
 */
function parseArchive ($) {
  var promises = []
  const apps = $('#selectedcontent li > a').get().map(function (app) {
    let url = app.attribs.href
    let title = app.children[0].data.trim()
    let id = parseFloat(url.match(/id(\d)+/)[0].substring(2))

    return {
      id: id,
      title: title,
      url: url
    }
  })

  const prev = $('#selectedgenre .paginate-previous').get()
  const next = $('#selectedgenre .paginate-more').get()

  promises.push(apps)
  promises.push(prev)
  promises.push(next)

  return Promise.all(promises)
    .then((data) => {
      var response = {
        results: data[0]
      }

      // If we have a previous button, add the pagination to the response
      if (data[1] !== undefined && data[1][0] !== undefined && data[1][0].attribs.href !== undefined) {
        response.prev = data[1][0].attribs.href
      }

      // If we have a next button, add the pagination to the response
      if (data[2] !== undefined && data[2][0] !== undefined && data[2][0].attribs.href !== undefined) {
        response.next = data[2][0].attribs.href
      }

      return response
    })
}

module.exports = getParseArchive;
