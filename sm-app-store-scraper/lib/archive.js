'use strict'

const request = require('./../../node_modules/google-play-scraper/lib/utils/request')
const cheerio = require('cheerio')
const store = require('app-store-scraper')
const R = require('ramda')
const getParseArchive = require('./utils/parseArchive');

function archive (opts) {
  return new Promise(function (resolve, reject) {
    opts = R.clone(opts || {})
    validate(opts)

    const options = {
      url: buildUrl(opts),
      proxy: opts.proxy
    }

    request(options, opts.throttle)
      .then(cheerio.load)
      .then(getParseArchive(opts))
      .then(resolve)
      .catch(reject)
  })
}

function validate (opts) {
  if (opts === undefined) {
    throw Error('Invalid category.')
  }

  opts.category = Number(opts.category)

  if (opts.category && !R.contains(opts.category, R.values(store.category))) {
    throw Error('Invalid category ' + opts.category)
  }

  if (opts.letter) {
    opts.letter = opts.letter.toUpperCase()
    var validLetter = new RegExp("[A-Z\\*]").test(opts.letter)
    if (validLetter !== true) {
      throw Error('The letter attribute must be A-Z, *, or empty')
    }
  } else {
    opts.letter = ''
  }

  if (opts.page) {
    opts.page = Number(opts.page) || 0
    if (opts.page > 100) {
      throw Error('The maximum page index is 100')
    }
  } else {
    opts.page = ''
  }
}

function buildUrl (opts) {
  let url = 'https://itunes.apple.com/us/genre/category'

  url += `/id${opts.category}`
  url += `?mt=8&letter=${opts.letter}&page=${opts.page}`

  return url
}

module.exports = archive
