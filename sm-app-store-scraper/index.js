'use strict';

const store = require('app-store-scraper')

module.exports.category = store.category
module.exports.collection = store.collection
module.exports.device = store.device
module.exports.sort = store.sort

module.exports.app = store.app
module.exports.list = require('./lib/list')
module.exports.search = store.search
module.exports.suggest = store.suggest
module.exports.similar = store.similar
module.exports.reviews = store.reviews
module.exports.archive = require('./lib/archive')
