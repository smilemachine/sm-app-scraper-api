const express = require('express')
const fs = require('fs')
const app = express()

// Health check
app.get('/', function(req, res) {
  res.send('Health check.')
})

// Load in the App and Play store routes
require('./store-routes')(app, 'google')
require('./store-routes')(app, 'apple')

// Launch the app
app.listen(8080, function() {
  console.log('Listening on port 8080')
})
