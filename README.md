# README #

This is a small NodeJS api which scrapes the Google Play and Apple App Store websites. Note that this scrapes on-demand, so please poll with caution as you may get your IP blocked.

### How do I get set up? ###

* Clone the repo to your local computer
* Run `npm install`
* Run `node index.js`
* View in your browser at http://localhost:8080

### Endpoints ###

* Supported stores are currently `/apple` and `/google`
* `/{store}/apps?q=` to display a list of apps with optional parameters
* `/{store}/apps?collection=&category=&num=` to display a list of apps with optional parameters
* `/{store}/categories` to display a list of categories
* `/{store}/collection` to display a list of collections
* `/apple/archive/{category}?letter=&page=` to display a list of apps from the Apple archive
