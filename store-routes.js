'use strict'

const express = require('express')
const path = require('path')
const qs = require('querystring')

const router = express()
const toList = (apps) => ({results: apps})
const cleanUrls = (prefix, req) => (app) => Object.assign({}, app, {
  playstoreUrl: app.url,
  url: buildUrl(req, prefix, 'apps/' + app.appId),
  permissions: buildUrl(req, prefix, 'apps/' + app.appId + '/permissions'),
  similar: buildUrl(req, prefix, 'apps/' + app.appId + '/similar'),
  reviews: buildUrl(req, prefix, 'apps/' + app.appId + '/reviews'),
  developer: {
    devId: app.developer,
    url: buildUrl(req, prefix, 'developers/' + qs.escape(app.developer))
  }
})

const buildUrl = (req, prefix, subpath) =>
  req.protocol + '://' + path.join(req.get('host'), req.baseUrl, '/' + prefix + '/' + subpath)

function errorHandler (err, req, res, next) {
  res.status(400).json({message: err.message})
  next()
}

module.exports = function(router, prefix) {

  /**
   * Determine which store to use
   */
  var store = null

  if (prefix == 'google') {
    store = require('google-play-scraper')
  } else if (prefix === 'apple') {
    store = require('./sm-app-store-scraper')
  } else {
    console.log('Unsupported app store: ' + prefix)
    return
  }


  /**
   * Provide a list of available list endpoints
   */
  router.get('/' + prefix + '/', (req, res) =>
    res.json({
      apps: buildUrl(req, prefix, 'apps'),
      collections: buildUrl(req, prefix, 'collections'),
      categogries: buildUrl(req, prefix, 'categories')
    }))


  /**
   * Get a list of apps with optional parameters
   *  ?q=SEARCH_TERM
   */
  router.get('/' + prefix + '/apps/', function (req, res, next) {
    if (!req.query.q) {
      return next()
    }

    const opts = Object.assign({term: req.query.q}, req.query)

    if (store.search !== undefined) {
      store.search(opts)
        .then((apps) => apps.map(cleanUrls(prefix, req)))
        .then(toList)
        .then(res.json.bind(res))
        .catch(next)
    } else {
      res.status(400).json({message:'Search not supported in this store.'})
    }
  })


  /**
   * Search suggest
   */
  router.get(prefix + '/apps/', function (req, res, next) {
    if (!req.query.suggest) {
      return next()
    }

    const toJSON = (term) => ({
      term,
      url: buildUrl(req, prefix, 'apps/') + '?' + qs.stringify({q: term})
    })

    if (store.suggest !== undefined) {
      store.suggest({term: req.query.suggest})
        .then((terms) => terms.map(toJSON))
        .then(toList)
        .then(res.json.bind(res))
        .catch(next)
    } else {
      res.status(400).json({message:'Suggest not supported in this store.'})
    }
  })


  /**
   * Get a list of apps with optional parameters
   *  ?category=CATEGORY
   *  ?collection=COLLECTION
   *  ?num=1-100
   */
  router.get('/' + prefix + '/apps/', function (req, res, next) {
    function paginate (apps) {
      const num = parseInt(req.query.num || '60')
      const start = parseInt(req.query.start || '0')

      if (start - num >= 0) {
        req.query.start = start - num
        apps.prev = buildUrl(req, prefix, '/apps/') + '?' + qs.stringify(req.query)
      }

      if (start + num <= 500) {
        req.query.start = start + num
        apps.next = buildUrl(req, prefix, '/apps/') + '?' + qs.stringify(req.query)
      }

      return apps
    }

    if (store.app !== undefined) {
      // Apple needs the category as a number
      if (prefix === 'apple' && req.query.category !== undefined) {
        req.query.category = Number(req.query.category)
      }

      store.list(req.query)
        .then((apps) => apps.map(cleanUrls(prefix, req)))
        .then(toList).then(paginate)
        .then(res.json.bind(res))
        .catch(next)
    } else {
      res.status(400).json({message:'List not supported in this store.'})
    }
  })


  /**
   * Get details for a specific app
   */
  router.get('/' + prefix + '/apps/:appId', function (req, res, next) {
    const opts = Object.assign({appId: req.params.appId}, req.query)
    opts.id = opts.appId
    
    if (store.app !== undefined) {
      store.app(opts)
        .then(cleanUrls(prefix, req))
        .then(res.json.bind(res))
        .catch(next)
    } else {
      res.status(400).json({message:'App not supported in this store.'})
    }
  })


  /**
   * Get a list of similar apps for a specific app
   */
  router.get('/' + prefix + '/apps/:appId/similar', function (req, res, next) {
    const opts = Object.assign({appId: req.params.appId}, req.query)

    if (store.similar !== undefined) {
      store.similar(opts)
        .then((apps) => apps.map(cleanUrls(prefix, req)))
        .then(toList)
        .then(res.json.bind(res))
        .catch(next)
    } else {
      res.status(400).json({message:'Similar not supported in this store.'})
    }
  })


  /**
   * Get a list of permissions for a specific app
   */
  router.get('/' + prefix + '/apps/:appId/permissions', function (req, res, next) {
    const opts = Object.assign({appId: req.params.appId}, req.query)

    if (store.permissions !== undefined) {
      store.permissions(opts)
        .then(toList)
        .then(res.json.bind(res))
        .catch(next)
    } else {
      res.status(400).json({message:'Permissions not supported in this store.'})
    }
  })


  /**
   * Get a list of reviews for a specific app
   */
  router.get('/' + prefix + '/apps/:appId/reviews', function (req, res, next) {
    function paginate (apps) {
      const page = parseInt(req.query.page || '0')
      const subpath = '/' + prefix + '/apps/' + req.params.appId + '/reviews/'

      if (page > 0) {
        req.query.page = page - 1
        apps.prev = buildUrl(req, subpath) + '?' + qs.stringify(req.query)
      }

      if (apps.results.length) {
        req.query.page = page + 1
        apps.next = buildUrl(req, subpath) + '?' + qs.stringify(req.query)
      }

      return apps
    }

    const opts = Object.assign({appId: req.params.appId}, req.query)

    if (store.reviews !== undefined) {
      store.reviews(opts)
        .then(toList)
        .then(paginate)
        .then(res.json.bind(res))
        .catch(next)
    } else {
      res.status(400).json({message:'Reviews not supported in this store.'})
    }
  })


  /**
   * Get a list of apps for a specific developer
   */
  router.get('/' + prefix + '/developers/:devId/', function (req, res, next) {
    const opts = Object.assign({devId: req.params.devId}, req.query)

    if (store.developer !== undefined) {
      store.developer(opts)
        .then((apps) => apps.map(cleanUrls(prefix, req)))
        .then((apps) => ({
          devId: req.params.devId,
          apps
        }))
        .then(res.json.bind(res))
        .catch(next)
      } else {
        res.status(400).json({message:'Developers not supported in this store.'})
      }
  })


  /**
   * A list of developers is unsupported, inform the user that extra info is required
   */
  router.get('/' + prefix + '/developers/', (req, res) =>
    res.status(400).json({
      message: 'Please specify a developer id.',
      example: buildUrl(req, prefix, '/developers/' + qs.escape('DxCo Games'))
    }))


  /**
   * Get a list of supported collections for this store
   */
  router.get('/' + prefix + '/collections', function (req, res, next) {
    if (store.collection !== undefined) {
      res.json(store.collection)
    } else {
      res.status(400).json({message:'Collections not supported in this store.'})
    }
  })


  /**
   * Get a list of supported categories for this store
   */
  router.get('/' + prefix + '/categories', function (req, res, next) {
    if (store.category !== undefined) {
      res.json(store.category)
    } else {
      res.status(400).json({message:'Categories not supported in this store.'})
    }
  })


  /**
   * Get an archive of apps for the Apple App Store with optional parameters
   *  ?letter=A-Z, * for number, or nothing for Popular Apps
   *  ?page=#
   */
  router.get('/' + prefix + '/archive/:category', function (req, res, next) {
    const opts = Object.assign({category: req.params.category}, req.query)

    if (store.archive !== undefined) {
      store.archive(opts)
        .then((data) => {
          // Clean the app urls
          data.results = data.results.map((app) => Object.assign({}, app, {
            url: buildUrl(req, prefix, 'apps/' + app.id)
          }))

          // Clean the previous url
          if (data.prev !== undefined) {
            var url = buildUrl(req, prefix, '/archive/' + req.params.category)
            var params = (data.prev.match(/(\?[^\n]+)$/g) || [])[0]
            data.prev = url + params
          }

          // Clean the next url
          if (data.next !== undefined) {
            var url = buildUrl(req, prefix, '/archive/' + req.params.category)
            var params = (data.next.match(/(\?[^\n]+)$/g) || [])[0]
            data.next = url + params
          }

          return data
        })
        .then(res.json.bind(res))
        .catch(next)
    } else {
      res.status(400).json({message:'Archive not supported in this store.'})
    }
  })


  /**
   * Use a custom error handler
   */
  router.use(errorHandler)
}
